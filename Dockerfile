# Upstream direct image
FROM provectuslabs/kafka-ui:v0.7.1

MAINTAINER Ignacio Coterillo <icoteril@cern.ch>

USER root
RUN mkdir /config 

COPY etc/krb5.conf /etc

# Incorporate CERN bundle into system wide cacert
COPY artifacts/CERN-bundle.pem /usr/local/share/ca-certificates/
WORKDIR /usr/local/share/ca-certificates
RUN apk add ca-certificates
RUN awk 'BEGIN {c=0;} /BEGIN CERT/{c++} { print > "cerncert." c ".pem"}' < CERN-bundle.pem && rm CERN-bundle.pem
RUN update-ca-certificates
RUN apk add java-cacerts
# Override embedded cacert, used for Kafka Connect connections
RUN cp /etc/ssl/certs/java/cacerts /usr/lib/jvm/zulu17-ca/lib/security/cacerts
WORKDIR /

USER kafkaui


